package net.javaguides.springboot.unitarios.util;

import net.javaguides.springboot.model.Employee;

public class EmployeeCreator {
    public static Employee createValidEmployee(){
        return Employee.builder()
                .id(1L)
                .firstName("Caio")
                .lastName("Nunes")
                .emailId("teste@teste.com")
                .build();
    }
}
