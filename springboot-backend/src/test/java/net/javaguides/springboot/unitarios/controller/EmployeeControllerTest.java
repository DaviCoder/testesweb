package net.javaguides.springboot.unitarios.controller;

import net.javaguides.springboot.controller.EmployeeController;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.Employee;
import net.javaguides.springboot.repository.EmployeeRepository;
import net.javaguides.springboot.unitarios.util.EmployeeCreator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@RunWith(PowerMockRunner.class)
public class EmployeeControllerTest {

    @InjectMocks
    private EmployeeController employeeController;

    @Mock
    private EmployeeRepository employeeRepository;

    @BeforeEach
    public void setUp() {
        List<Employee> lista = new ArrayList<>(List.of(EmployeeCreator.createValidEmployee()));
        PowerMockito.when(employeeRepository.findAll()).thenReturn(lista);
    }

    @Test
    public void testCreateEmployee() {
        Employee employee = EmployeeCreator.createValidEmployee();
        PowerMockito.when(employeeRepository.save(employee)).thenReturn(employee);

        Employee createdEmployee = employeeController.createEmployee(employee);
        Assertions.assertAll(() -> Assertions.assertEquals(employee.getFirstName(), createdEmployee.getFirstName()),
                () -> Assertions.assertEquals(employee.getLastName(), createdEmployee.getLastName()));
    }

    @Test
    public void testGetAllEmployees() {
        List<Employee> expectedList = List.of(EmployeeCreator.createValidEmployee());
        List<Employee> employeeList = employeeController.getAllEmployees();
        Assertions.assertEquals(expectedList.size(), employeeList.size());
    }

    @Test
    public void testGetEmployeeById() {
        Long id = 1L;
        Employee employee = EmployeeCreator.createValidEmployee();
        PowerMockito.when(employeeRepository.findById(id)).thenReturn(Optional.of(employee));

        ResponseEntity<Employee> employeeResponseEntity = employeeController.getEmployeeById(1L);
        Assertions.assertEquals(HttpStatus.OK, employeeResponseEntity.getStatusCode());

        Employee responseBody = employeeResponseEntity.getBody();
        Assertions.assertNotNull(responseBody);
        Assertions.assertEquals(employee.getEmailId(), responseBody.getEmailId());
    }

    @Test
    public void testTryGetEmployeeWhenInvalidId() {
        Long expectedId = 1L;
        Long invalidId = 3L;

        Employee employee = EmployeeCreator.createValidEmployee();
        PowerMockito.when(employeeRepository.findById(expectedId)).thenReturn(Optional.of(employee));

        Assertions.assertThrows(ResourceNotFoundException.class, () -> employeeController.getEmployeeById(invalidId));
    }

    @Test
    public void testPostEmployeeWhenInvalidId() {
        Long expectedId = 1L;
        Long invalidId = 3L;

        Employee employee = EmployeeCreator.createValidEmployee();
        PowerMockito.when(employeeRepository.findById(expectedId)).thenReturn(Optional.of(employee));

        Assertions.assertThrows(ResourceNotFoundException.class, () -> employeeController.updateEmployee(invalidId, employee));
    }

    @Test
    public void testPutEmployee() {
        Long expectedId = 1L;

        Employee employee = EmployeeCreator.createValidEmployee();
        PowerMockito.when(employeeRepository.findById(expectedId)).thenReturn(Optional.of(employee));
        PowerMockito.when(employeeRepository.save(employee)).thenReturn(employee);

        ResponseEntity<Employee> employeeResponseEntity = employeeController.updateEmployee(expectedId, employee);
        Assertions.assertAll(() -> Assertions.assertEquals(HttpStatus.OK, employeeResponseEntity.getStatusCode()),
                () -> Assertions.assertEquals(expectedId, employee.getId()));

    }

    @Test
    public void testDeleteEmployee() {
        Long id = 1L;

        Employee employee = EmployeeCreator.createValidEmployee();
        PowerMockito.when(employeeRepository.findById(id)).thenReturn(Optional.of(employee));

        ResponseEntity<Map<String, Boolean>> response = employeeController.deleteEmployee(id);

        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testTryDeleteEmployeeWhenInvalidId() {
        Long existId = 1L;
        Long unexistId = 3L;

        Employee employee = EmployeeCreator.createValidEmployee();
        PowerMockito.when(employeeRepository.findById(existId)).thenReturn(Optional.of(employee));

        Assertions.assertThrows(ResourceNotFoundException.class, () -> employeeController.deleteEmployee(unexistId));
    }

}
