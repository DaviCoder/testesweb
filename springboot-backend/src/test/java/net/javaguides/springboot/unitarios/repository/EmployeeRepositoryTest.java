package net.javaguides.springboot.unitarios.repository;

import net.javaguides.springboot.model.Employee;
import net.javaguides.springboot.repository.EmployeeRepository;
import net.javaguides.springboot.unitarios.util.EmployeeCreator;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

@DataJpaTest
public class EmployeeRepositoryTest {
    @Autowired
    private EmployeeRepository employeeRepository;

    Employee employee;
    Employee savedEmployee;
    @BeforeEach
    public void setUp(){
        employee = EmployeeCreator.createValidEmployee();
        savedEmployee = employeeRepository.save(employee);
    }

    @Test
    public void save_PersistEmployee(){
        Assertions.assertThat(savedEmployee.getFirstName()).isEqualTo(employee.getFirstName());
    }


    @Test
    public void updateEmployee(){
        savedEmployee.setFirstName("Nome");
        savedEmployee.setLastName("Sobrenome");
        savedEmployee.setEmailId("teste@teste.com");
        Employee employeeUpdate = employeeRepository.save(savedEmployee);

        Assertions.assertThat(employeeUpdate.getFirstName()).isEqualTo(savedEmployee.getFirstName());
        Assertions.assertThat(employeeUpdate.getLastName()).isEqualTo(savedEmployee.getLastName());
        Assertions.assertThat(employeeUpdate.getEmailId()).isEqualTo(savedEmployee.getEmailId());
    }

    @Test
    public void deleteEmployee(){
        employeeRepository.delete(savedEmployee);
        Optional<Employee> employeeOptional = employeeRepository.findById(savedEmployee.getId());

        Assertions.assertThat(employeeOptional).isEmpty();
    }
}
