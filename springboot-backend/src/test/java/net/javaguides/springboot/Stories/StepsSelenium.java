package net.javaguides.springboot.Stories;


import org.jbehave.core.annotations.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class StepsSelenium {

    private String urlAddEmployee = "http://localhost:3000/add-employee/_add";
    private String urlHome = "http://localhost:3000/";
    private WebDriver driver;

    @BeforeStory
    public void setUp() {
        System.setProperty("webdriver.edge.driver",System.getProperty("user.dir") + "\\src\\test\\java\\resources\\msedgedriver.exe");
        EdgeOptions options = new EdgeOptions();
        options.addArguments("--allowed-ips");
        options.addArguments("--no-sandbox");
        options.addArguments("--window-size=1280x768");
        options.addArguments("--disable-extensions");
        options.addArguments("--ignore-certificate-errors");
        options.addArguments("--disable-notifications");
        options.addArguments("--disable-infobars");
        options.addArguments("--allow-running-insecure-content");
        options.addArguments("--disable-popup-blocking");
        options.addArguments("--remote-allow-origins=*");
        options.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);

        driver = new EdgeDriver(options);
    }

    @AfterStory
    public void tearDown() {
        try {
            driver.quit();
        } catch (Exception ignored) {}
        try {
            driver.close();
        } catch (Exception ignored) {}
    }
    @Given("I am on the home page")
    public void givenIAmOnHomePage() {
        driver.get(urlHome);
        dormirRapidinho();
    }

    @When("I click on the \"$buttonXPath\" button")
    public void whenIClickOnButton(String buttonXPath) {
        WebElement button = driver.findElement(By.xpath(buttonXPath));
        button.click();
        dormirRapidinho();
    }

    @Then("I should be redirected to the register page")
    public void thenIShouldBeRedirectedToRegisterPage() {
        String actualUrl = driver.getCurrentUrl();
        assertEquals(urlAddEmployee, actualUrl);
    }

    @Given("I am on the register page")
    public void givenIAmOnRegisterPage() {
        driver.get(urlAddEmployee);
        dormirRapidinho();
    }

    @When("I fill in \"$elementXPath\" with \"$value\"")
    public void whenIFillInField(String elementXPath, String value) {
        WebElement element = driver.findElement(By.xpath(elementXPath));
        element.sendKeys(value);
        dormirRapidinho();
    }

    @Then("I validate the alert message \"$alertMessage\"")
    public void thenIValidateTheAlertMessage(String alertMessage) {
        String actualAlertMessage = driver.switchTo().alert().getText();
        assertEquals(alertMessage, actualAlertMessage);
    }

    @Then("I finish the test")
    public void thenIFinishTheTest() {
        driver.quit();
    }

    private void dormirRapidinho(){
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
